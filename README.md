# ArchLabs Installer

#### Features

- Minimal dependencies.
- LUKS and/or LVM support.
- Btrfs and subvolume support.
- Full device auto partitioning.
- Mirror country selection and sorting.
- Self updating with persistent settings.
- Background base install process while you customize.
- Choose kernel, file system, bootloader, packages, sessions, shell, login manager, etc.


#### Requirements

- `awk`
- `sed`
- `curl`
- `dialog`
- `parted`
- `coreutils`
- `findutils`
- `util-linux`
- `arch-install-scripts`


#### Manual Installation

```
curl -fSL https://bitbucket.org/archlabslinux/installer/raw/master/installer -o /usr/bin/installer
```

---

A packaged version can also be found in our repos:

- stable: https://bitbucket.org/archlabslinux/archlabs\_repo

- unstable: https://bitbucket.org/archlabslinux/archlabs\_unstable

